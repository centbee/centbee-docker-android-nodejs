# Centbee Android-Nodejs image

### Pull newest build from Docker Hub
```
docker pull centbee/android-nodejs:latest
```

### Run image
```
docker run -it centbee/android-nodejs:latest bash
```

### Use as base image
```Dockerfile
FROM centbee/android-nodejs:latest
```
